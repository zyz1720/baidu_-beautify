
// 插件开启(默认样式)
const switchOn = document.querySelector('#on-but');//左边圆
const switchline = document.querySelector('#back');//左边圆
const switchoff = document.querySelector('#off-but');//右边圆
const switchtext = document.querySelector('#on-tips');//文字

// 点击开启插件
switchOn.onclick = async () => {
    switchOn.style.display = "none";
    switchline.style.backgroundColor = "rgba(67, 111, 246, 0.12)";
    switchoff.style.display = "flex";
    switchtext.innerText = " 插件已开启";

    document.querySelector('.options-box').style.display = "block";
    const [tab] = await chrome.tabs.query({ active: true, currentWindow: true });
    chrome.scripting.executeScript({
        target: { tabId: tab.id },
        //args: [{ state }],
        function: (event) => {
            document.querySelector('#content_right').style.display = "none";
            document.querySelector('#s_tab').style.paddingTop = "80px";
            if (document.querySelector('#searchTag')) {
                document.querySelector('#searchTag').style.display = "none";
            }
            const container = document.querySelector('#container')
            const content_left = document.querySelector('#content_left');
            const ture_container = document.getElementsByClassName('result');
            const result_op = document.getElementsByClassName('result-op');
            document.body.style.backgroundColor = "#F5F6F7 ";

            container.style.display = "block";
            container.style.justifyContent = "center";

            content_left.style.display = "flex";
            content_left.style.width = "1200px";
            content_left.style.justifyContent = "space-between";
            content_left.style.flexWrap = "wrap";
            content_left.style.margin = "0";
            content_left.style.padding = "0";

            for (let i = 0; i < ture_container.length; i++) {
                const element = ture_container[i];
                element.style.marginBottom = " 16px";
                element.style.padding = " 16px";
                element.style.border = " 1px solid #EEEEEE";
                element.style.boxShadow = "0px 2px 16px 0px rgba(221,221,221,1)";
                element.style.borderRadius = "8px";
                element.style.backgroundColor = "#FFFFFF"
            }
            for (let i = 0; i < result_op.length; i++) {
                const element = result_op[i];
                element.style.display = " none";
            }

            const state = {
                cardShow: true,//显示设置
                allFlag: true, // 插件开关显示
                eveFlag: true, // 双列显示
                cardFlag: true, // 卡片显示
                serFlag: false, // 大家搜
                tranFlag: false, // 透明显示
                bgColor: "#F5F6F7",
                bgImg: "../image/paimen.jpg"
            };
            const style = {
                content_right: "none",
                result_op: "none",
                s_tab: "80px",
                container_display: "block",
                con_jus: "center",
                conleft_dis: "flex",
                conleft_width: "1200px",
                conleft_jus: "space-between",
                conleft_flexwarp: "wrap",
                conleft_margin: "0",
                conleft_padding: "0",
                res_marbottom: " 16px",
                res_padding: "16px",
                res_border: " 1px solid #EEEEEE",
                res_boxShadow: "0px 2px 16px 0px rgba(221,221,221,1)",
                res_borderRadius: "8px",
                res_backgroundColor: "#FFFFFF"
            }

            let popsData = {}

            popsData.Switch = "ON";
            popsData.State = state;
            popsData.Style = style;

            chrome.runtime.sendMessage({ name: 'init', data: popsData }, function (res) {
                console.log(res.msg);
            })
        },
    });
}
switchoff.onclick = () => {
    switchOn.style.display = "flex";
    switchline.style.backgroundColor = "#Bdc1c6";
    switchoff.style.display = "none";
    switchtext.innerText = " 插件已关闭";
    document.querySelector('.options-box').style.display = "none";

    let popsData = {}
    popsData.Switch = "OFF";

    chrome.runtime.sendMessage({ name: 'change', data: popsData }, function (res) {
        console.log(res.msg);
    })

}

// 双列模式
const switcheveOn = document.querySelector('#on-but-eve');//左边圆
const switcheveline = document.querySelector('#back-eve');//左边圆
const switcheveoff = document.querySelector('#off-but-eve');//右边圆
switcheveOn.onclick = async () => {
    switcheveOn.style.display = "none";
    switcheveline.style.backgroundColor = "rgba(67, 111, 246, 0.12)";
    switcheveoff.style.display = "block";

    const [tab] = await chrome.tabs.query({ active: true, currentWindow: true });
    chrome.scripting.executeScript({
        target: { tabId: tab.id },
        //args: [{ btnColor, index }],  
        function: (event) => {
            const content_left = document.querySelector('#content_left');

            content_left.style.display = "flex";
            content_left.style.width = "1200px";
            content_left.style.justifyContent = "space-between";
            content_left.style.flexWrap = "wrap";
            content_left.style.margin = "0";
            content_left.style.padding = "0";

            let popsData = {}
            popsData.State = {
                eveFlag: true
            }
            popsData.Style = {
                conleft_dis: "flex",
                conleft_width: "1200px",
                conleft_jus: "space-between",
                conleft_flexwarp: "wrap",
                conleft_margin: "0",
                conleft_padding: "0",
            }
            chrome.runtime.sendMessage({ name: 'change', data: popsData }, function (res) {
                console.log(res.msg);
            })

        },
    });
}
switcheveoff.onclick = async () => {
    switcheveOn.style.display = "block";
    switcheveline.style.backgroundColor = "#Bdc1c6";
    switcheveoff.style.display = "none";

    const [tab] = await chrome.tabs.query({ active: true, currentWindow: true });
    chrome.scripting.executeScript({
        target: { tabId: tab.id },
        //args: [{ btnColor, index }],  
        function: (event) => {
            const content_left = document.querySelector('#content_left');

            content_left.style.display = "flex";
            content_left.style.width = "1100px";
            content_left.style.justifyContent = "center";
            content_left.style.flexWrap = "wrap";
            content_left.style.margin = "0";
            content_left.style.padding = "0";

            let popsData = {}
            popsData.State = {
                eveFlag: false
            }
            popsData.Style = {
                conleft_dis: "flex",
                conleft_width: "1100px",
                conleft_jus: "center",
                conleft_flexwarp: "wrap",
                conleft_margin: "0",
                conleft_padding: "0",
            }
            chrome.runtime.sendMessage({ name: 'change', data: popsData }, function (res) {
                console.log(res.msg);
            })
        },
    });
}

// 卡片风格
const switchcardOn = document.querySelector('#on-but-card');//左边圆
const switchcardline = document.querySelector('#back-card');//左边圆
const switchcardoff = document.querySelector('#off-but-card');//右边圆
switchcardOn.onclick = async () => {
    switchcardOn.style.display = "none";
    switchcardline.style.backgroundColor = "rgba(67, 111, 246, 0.12)";
    switchcardoff.style.display = "block";

    const [tab] = await chrome.tabs.query({ active: true, currentWindow: true });
    chrome.scripting.executeScript({
        target: { tabId: tab.id },
        //args: [{ btnColor, index }],  
        function: (event) => {
            const ture_container = document.getElementsByClassName('result');
            for (let i = 0; i < ture_container.length; i++) {
                const element = ture_container[i];
                element.style.marginBottom = " 16px";
                element.style.padding = " 16px";
                element.style.border = " 1px solid #EEEEEE";
                element.style.boxShadow = "0px 2px 16px 0px rgba(221,221,221,1)";
                element.style.borderRadius = "8px";
                element.style.backgroundColor = "#FFFFFF"
            }

            let popsData = {}
            popsData.State = {
                cardFlag: true
            }
            popsData.Style = {
                res_marbottom: " 16px",
                res_padding: "16px",
                res_border: " 1px solid #EEEEEE",
                res_boxShadow: "0px 2px 16px 0px rgba(221,221,221,1)",
                res_borderRadius: "8px",
                res_backgroundColor: "#FFFFFF"
            }
            chrome.runtime.sendMessage({ name: 'change', data: popsData }, function (res) {
                console.log(res.msg);
            })
        },
    });
}
switchcardoff.onclick = async () => {
    switchcardOn.style.display = "block";
    switchcardline.style.backgroundColor = "#Bdc1c6";
    switchcardoff.style.display = "none";

    const [tab] = await chrome.tabs.query({ active: true, currentWindow: true });
    chrome.scripting.executeScript({
        target: { tabId: tab.id },
        //args: [{ btnColor, index }],  
        function: (event) => {
            const ture_container = document.getElementsByClassName('result');
            for (let i = 0; i < ture_container.length; i++) {
                const element = ture_container[i];
                element.style.marginBottom = " 16px";
                element.style.padding = " 16px";
                element.style.border = " none";
                element.style.boxShadow = "none";
                element.style.borderRadius = "0px";
                element.style.backgroundColor = "inherit"
            }
            let popsData = {}
            popsData.State = {
                cardFlag: false
            }
            popsData.Style = {
                res_marbottom: " 16px",
                res_padding: "16px",
                res_border: " none",
                res_boxShadow: "none",
                res_borderRadius: "0px",
                res_backgroundColor: "inherit"
            }
            chrome.runtime.sendMessage({ name: 'change', data: popsData }, function (res) {
                console.log(res.msg);
            })
        },
    });
}

// 大家搜
const switchSerOn = document.querySelector('#on-but-sertips');//左边圆
const switchSerline = document.querySelector('#back-sertips');//左边圆
const switchSeroff = document.querySelector('#off-but-sertips');//右边圆
switchSerOn.onclick = async () => {
    switchSerOn.style.display = "none";
    switchSerline.style.backgroundColor = "rgba(67, 111, 246, 0.12)";
    switchSeroff.style.display = "block";

    const [tab] = await chrome.tabs.query({ active: true, currentWindow: true });
    chrome.scripting.executeScript({
        target: { tabId: tab.id },
        //args: [{ btnColor, index }],  
        function: (event) => {
            const result_op = document.getElementsByClassName('result-op');
            for (let i = 0; i < result_op.length; i++) {
                const element = result_op[i];
                element.style.display = " block";
            }

            let popsData = {}
            popsData.State = {
                serFlag: true
            }
            popsData.Style = {
                result_op: "block"
            }
            chrome.runtime.sendMessage({ name: 'change', data: popsData }, function (res) {
                console.log(res.msg);
            })
        },
    });
}
switchSeroff.onclick = async () => {
    switchSerOn.style.display = "block";
    switchSerline.style.backgroundColor = "#Bdc1c6";
    switchSeroff.style.display = "none";

    const [tab] = await chrome.tabs.query({ active: true, currentWindow: true });
    chrome.scripting.executeScript({
        target: { tabId: tab.id },
        //args: [{ btnColor, index }],  
        function: (event) => {
            const result_op = document.getElementsByClassName('result-op');
            for (let i = 0; i < result_op.length; i++) {
                const element = result_op[i];
                element.style.display = " none";
            }

            let popsData = {}
            popsData.State = {
                serFlag: false
            }
            popsData.Style = {
                result_op: "none"
            }
            chrome.runtime.sendMessage({ name: 'change', data: popsData }, function (res) {
                console.log(res.msg);
            })
        },
    });
}

//卡片透明
const switchtranOn = document.querySelector('#on-but-tran');//左边圆
const switchtranline = document.querySelector('#back-tran');//左边圆
const switchtranoff = document.querySelector('#off-but-tran');//右边圆
switchtranOn.onclick = async () => {
    switchtranOn.style.display = "none";
    switchtranline.style.backgroundColor = "rgba(67, 111, 246, 0.12)";
    switchtranoff.style.display = "block";

    const [tab] = await chrome.tabs.query({ active: true, currentWindow: true });
    chrome.scripting.executeScript({
        target: { tabId: tab.id },
        //args: [{ btnColor, index }],  
        function: (event) => {
            const ture_container = document.getElementsByClassName('result');
            for (let i = 0; i < ture_container.length; i++) {
                const element = ture_container[i];
                element.style.backgroundColor = "#ffffff42";
                element.style.border = " none";
                element.style.boxShadow = "none";
            }
            let popsData = {}
            popsData.State = {
                tranFlag: true
            }
            popsData.Style = {
                res_border: " none",
                res_boxShadow: "none",
                res_backgroundColor: "#ffffff42"
            }
            chrome.runtime.sendMessage({ name: 'change', data: popsData }, function (res) {
                console.log(res.msg);
            })
        },
    });
}
switchtranoff.onclick = async () => {
    switchtranOn.style.display = "block";
    switchtranline.style.backgroundColor = "#Bdc1c6";
    switchtranoff.style.display = "none";

    const [tab] = await chrome.tabs.query({ active: true, currentWindow: true });
    chrome.scripting.executeScript({
        target: { tabId: tab.id },
        //args: [{ btnColor, index }],  
        function: (event) => {
            const ture_container = document.getElementsByClassName('result');
            for (let i = 0; i < ture_container.length; i++) {
                const element = ture_container[i];
                element.style.backgroundColor = "#FFFFFF"
                element.style.border = " 1px solid #EEEEEE";
                element.style.boxShadow = "0px 2px 16px 0px rgba(221,221,221,1)";
            }
            let popsData = {}
            popsData.State = {
                tranFlag: false
            }
            popsData.Style = {
                res_border: " 1px solid #EEEEEE",
                res_boxShadow: "0px 2px 16px 0px rgba(221,221,221,1)",
                res_backgroundColor: "#FFFFFF"
            }
            chrome.runtime.sendMessage({ name: 'change', data: popsData }, function (res) {
                console.log(res.msg);
            })
        },
    });
}


//背景颜色
const CheckBgcolor = document.querySelector('#on-but-bgcolor');
const Color = document.querySelector('#color');
Color.onchange = async () => {
    CheckBgcolor.style.backgroundColor = Color.value;
    const value = Color.value;
    const [tab] = await chrome.tabs.query({ active: true, currentWindow: true });
    chrome.scripting.executeScript({
        target: { tabId: tab.id },
        args: [{ value }],  // // 无法访问外面的数据，args 可传递外面的数据 
        function: (event) => {
            document.body.style.backgroundImage = 'url(#)';
            document.body.style.backgroundColor = event.value;

            let popsData = {}
            popsData.State = {
                bgColor: event.value,
            }
            chrome.runtime.sendMessage({ name: 'change', data: popsData }, function (res) {
                console.log(res.msg);
            })
        },
    });

}

//背景图片
const CheckBgimg = document.querySelector('#Img');
const upLoad = document.querySelector('#upLoad');
upLoad.onchange = () => {
    var file = upLoad.files[0];
    var res = new FileReader();
    res.readAsDataURL(file);
    res.onload = async function (re) {
        CheckBgimg.src = re.target.result;
        const value = re.target.result;
        const [tab] = await chrome.tabs.query({ active: true, currentWindow: true });
        chrome.scripting.executeScript({
            target: { tabId: tab.id },
            args: [{ value }],  // // 无法访问外面的数据，args 可传递外面的数据 
            function: (event) => {
                document.body.style.backgroundAttachment = 'fixed';
                document.body.style.backgroundSize = '100% 100%';
                document.body.style.backgroundImage = 'url(' + event.value + ')';

                let popsData = {}
                popsData.State = {
                    bgImg: event.value,
                }
                console.log(popsData.State.bgImg);
                chrome.runtime.sendMessage({ name: 'change', data: popsData }, function (res) {
                    console.log(res.msg);
                })
            },
        });
    }
}

// 初始化插件
chrome.storage.sync.get('storeData', function (data) {
    console.log(data);
    if (data) {
        let newState = data.storeData.State;
        CheckBgcolor.style.backgroundColor = newState.bgColor;
        CheckBgimg.src = newState.bgImg;
        if (data.storeData.Switch == 'ON') {
            console.log(newState.tranFlag);
            if (newState.cardShow) {
                document.querySelector('.options-box').style.display = "block";
            } else {
                document.querySelector('.options-box').style.display = "none";
            }
            if (newState.allFlag) {
                switchOn.style.display = "none";
                switchline.style.backgroundColor = "rgba(67, 111, 246, 0.12)";
                switchoff.style.display = "flex";
                switchtext.innerText = " 插件已开启";
            } else {
                switchOn.style.display = "flex";
                switchline.style.backgroundColor = "#Bdc1c6";
                switchoff.style.display = "none";
                switchtext.innerText = " 插件已关闭";
            }
            if (newState.eveFlag) {
                switcheveOn.style.display = "none";
                switcheveline.style.backgroundColor = "rgba(67, 111, 246, 0.12)";
                switcheveoff.style.display = "block";
            } else {
                switcheveOn.style.display = "block";
                switcheveline.style.backgroundColor = "#Bdc1c6";
                switcheveoff.style.display = "none";
            }
            if (newState.cardFlag) {
                switchtranOn.style.display = "none";
                switchtranline.style.backgroundColor = "rgba(67, 111, 246, 0.12)";
                switchtranoff.style.display = "block";
            } else {
                switchcardOn.style.display = "block";
                switchcardline.style.backgroundColor = "#Bdc1c6";
                switchcardoff.style.display = "none";
            }
            if (newState.tranFlag) {
                switchtranOn.style.display = "none";
                switchtranline.style.backgroundColor = "rgba(67, 111, 246, 0.12)";
                switchtranoff.style.display = "block";
            } else {
                switchtranOn.style.display = "block";
                switchtranline.style.backgroundColor = "#Bdc1c6";
                switchtranoff.style.display = "none";
            }
            if (newState.serFlag) {
                switchSerOn.style.display = "none";
                switchSerline.style.backgroundColor = "rgba(67, 111, 246, 0.12)";
                switchSeroff.style.display = "block";
            } else {
                switchSerOn.style.display = "block";
                switchSerline.style.backgroundColor = "#Bdc1c6";
                switchSeroff.style.display = "none";
            }

        }
    }
});
