<!--
 * @Author: zyz 1720@qq.com
 * @Date: 2024-04-09 17:24:35
 * @LastEditors: zyz 1720@qq.com
 * @LastEditTime: 2024-04-09 17:31:02
 * @FilePath: \baidu_-beautify\README.md
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
-->

# Baidu Beautify
#### Name
Baidu search beautification tool
#### Introduction
A simple Baidu search beautification tool that defaults to a card style, dual column mode, removes ad recommendations, and some other custom options.
#### Permissions
No user permissions required
#### Default language
Simplified Chinese
#### Software type
Browser plugin