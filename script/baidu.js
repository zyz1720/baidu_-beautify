/*
 * @Author: zyz 1720@qq.com
 * @Date: 2024-04-01 15:03:38
 * @LastEditors: zyz 1720@qq.com
 * @LastEditTime: 2024-04-02 09:55:39
 * @FilePath: \baidu_-beautify\script\baidu.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
chrome.runtime.sendMessage({ name: 'style' }, function (res) {
    // console.log(res);
})

// 监听来自background.js的消息
chrome.runtime.onMessage.addListener(function (res, sender, sendResponse) {
    // console.log(res);
    setTimeout(() => {
        changeStyle(res);
    }, 900);
    sendResponse({ msg: '初始化样式数据成功！A_A' })
});


// 处理样式
function changeStyle(res) {
    if (res.data.Switch == "ON") {
        let newBg = res.data.State;
        let newStyle = res.data.Style;

        const container = document.querySelector('#container')
        const content_left = document.querySelector('#content_left');
        const ture_container = document.getElementsByClassName('result');
        const result_op = document.getElementsByClassName('result-op');

        document.body.style.backgroundColor = newBg.bgColor;
        document.body.style.backgroundImage = 'url(' + newBg.bgImg + ')';
        document.querySelector('#content_right').style.display = newStyle.content_right;
        if (document.querySelector('#searchTag')) {
            document.querySelector('#searchTag').style.display = "none";
        }
        document.querySelector('#s_tab').style.paddingTop = newStyle.s_tab;

        container.style.display = newStyle.container_display;
        container.style.justifyContent = newStyle.con_jus;

        content_left.style.display = newStyle.conleft_dis;
        content_left.style.width = newStyle.conleft_width;
        content_left.style.justifyContent = newStyle.conleft_jus;
        content_left.style.flexWrap = newStyle.conleft_flexwarp;
        content_left.style.margin = newStyle.conleft_margin;
        content_left.style.padding = newStyle.conleft_padding;

        for (let i = 0; i < ture_container.length; i++) {
            const element = ture_container[i];
            element.style.marginBottom = newStyle.res_marbottom;
            element.style.padding = newStyle.res_padding;
            element.style.border = newStyle.res_border;
            element.style.boxShadow = newStyle.res_boxShadow;
            element.style.borderRadius = newStyle.res_borderRadius;
            element.style.backgroundColor = newStyle.res_backgroundColor
        }
        for (let i = 0; i < result_op.length; i++) {
            const element = result_op[i];
            element.style.display = newStyle.result_op;
            element.style.marginBottom = newStyle.res_marbottom;
            element.style.padding = newStyle.res_padding;
            element.style.border = newStyle.res_border;
            element.style.boxShadow = newStyle.res_boxShadow;
            element.style.borderRadius = newStyle.res_borderRadius;
            element.style.backgroundColor = newStyle.res_backgroundColor
        }
    }
}