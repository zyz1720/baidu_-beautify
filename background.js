/*
 * @Author: zyz 1720@qq.com
 * @Date: 2024-04-01 15:03:38
 * @LastEditors: zyz 1720@qq.com
 * @LastEditTime: 2024-04-02 09:08:14
 * @FilePath: \baidu_-beautify\background.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
//和content.js和popup.js交互
chrome.runtime.onMessage.addListener(function (res, sender, sendResponse) {
    //console.log(res); //这里获取消息
    if (res.name == 'style') {
        chrome.storage.sync.get('storeData', function (newdata) {
            //console.log(newdata);
            if (newdata.storeData.Switch == 'ON') {
                sendResponse({ msg: '获取样式数据成功！O.o', data: newdata.storeData })
            } else {
                sendResponse({ msg: '插件已关闭QAQ！', data: newdata.storeData })
            }
        });
    }
    if (res.name == 'init') {
        chrome.storage.sync.set({ storeData: res.data }, function () {
            sendResponse({ msg: '初始化样式数据成功！A_A' })
        });
    }
    if (res.name == 'change') {
        let trueData = {};
        chrome.storage.sync.get('storeData', function (newdata) {
            trueData = formatModel(newdata.storeData, res.data);
            chrome.storage.sync.set({ storeData: trueData }, function () {
                sendResponse({ msg: '更改样式数据成功！H_H' })
            });
        });
    }
    return true;
})


// 监听地址栏变化
chrome.tabs.onUpdated.addListener(function (tabId, changeInfo, tab) {
    // console.log(tab);
    if (changeInfo.status == 'complete') {
        if (tab.url.includes('https://www.baidu.com/')) {
            chrome.storage.sync.get('storeData', function (newdata) {
                if (newdata.storeData.Switch == 'ON') {
                    chrome.tabs.sendMessage(tabId, { msg: '获取样式数据成功！O.o', data: newdata.storeData }, function (response) {
                        console.log(response);
                    });
                } else {
                    console.log('插件已关闭QAQ！');
                }
            });
        }
    } else {
        return;
    }

});
/*
 *格式
 */
const formatModel = (thisObj, sourceObj) => {
    for (var key in thisObj) {
        if (sourceObj && typeof (sourceObj[key]) != 'undefined') {
            if (thisObj[key] && Object.prototype.toString.call(thisObj[key]) === "[object Object]") {
                formatModel(thisObj[key], sourceObj[key]);
            } else {
                thisObj[key] = sourceObj[key];
            }
        }
    }
    return thisObj;
}

chrome.action.disable();
// 删除现有规则，只应用我们的规则
chrome.declarativeContent.onPageChanged.removeRules(undefined, () => {
    // 添加自定义规则
    chrome.declarativeContent.onPageChanged.addRules([
        {
            // 定义规则的条件
            conditions: [
                new chrome.declarativeContent.PageStateMatcher({
                    pageUrl: { urlPrefix: 'https://www.baidu.com/' },
                }),
            ],
            // 满足条件时显示操作
            actions: [new chrome.declarativeContent.ShowAction()],
        },
    ]);
});


